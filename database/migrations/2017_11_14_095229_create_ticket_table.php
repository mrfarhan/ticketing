<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issue_ticket', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject');
            $table->string('desc');
            $table->string('priority');
            $table->string('department');
            $table->string('status');
            $table->string('img');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('issue_ticket');
    }
}
