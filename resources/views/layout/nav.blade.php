<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <br>
                <div class="navbar nav_title" style="border: 0;">
                    <a href="/" class="site_title"><img src="{{url('/spc_Logo.png')}}" class="img-responsive"></a>
                </div>
                <div class="clearfix"></div>

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <br>
                    <div class="menu_section">
                        <ul class="nav side-menu">
                            <li class="active"><a href="/active" style="color: orange;"><i class="fa fa-pencil"></i> Active Tickets </a>
                            <li><a href="/complete" style="color: green;"><i class="fa fa-pencil"></i> Complete Tickets </a>
                            <li><a href="/create" style="color: white;"><i class="fa fa-pencil"></i> Create New </a>
                            <li><a href="/auth" style="color: white;"><i class="fa fa-gear"></i> Setting </a>
                        </ul>
                    </div>
                </div>
    <!-- /sidebar menu -->
    </div>
</div>

