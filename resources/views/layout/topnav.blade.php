<!-- top navigation --><br/>
<div class="top_nav">
    <div class="nav_menu">
        <nav>
                <div style="position: absolute;"><a href="/"><img src="{{url('/spc_Logo.png')}}" class="img-responsive"></a></div>
            <ul class="nav navbar-nav navbar-right">
                <li style="presentation">
                    <a href="/auth"><i class="fa fa-gear" style="color: green;"></i> Setting</a>
                </li>
                <li style="presentation">
                    <a href="/logout"><i class="fa fa-power-off" style="color: red;"></i> Logout </a>
                </li>
                <li style="presentation">
                    <a href="/complete"><i class="fa fa-pencil" style="color: green;"></i> Complete Tickets</a>
                </li>
                <li style="presentation">
                    <a href="/active"><i class="fa fa-pencil" style="color: orange;"></i> Active Tickets</a>
                </li>
                <li style="presentation">
                    <a href="/create"><i class="fa fa-pencil" style="color: black;"></i> Create New</a>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->