<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Spectrum Brands</title>
    <link href="{{url('/')}}/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{url('/')}}/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{url('/')}}/vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="{{url('/')}}/vendors/animate.css/animate.min.css" rel="stylesheet">
    <link href="{{url('/')}}/build/css/custom.min.css" rel="stylesheet">
