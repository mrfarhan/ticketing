@extends('layout')
        @section('xtra')
            <br>
            <link href="{{url('/')}}/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
            <link href="{{url('/')}}/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
            <link href="{{url('/')}}/vendorsnprogress/nprogress.css" rel="stylesheet">
            <link href="{{url('/')}}/vendorsiCheck/skins/flat/green.css" rel="stylesheet">
            <link href="{{url('/')}}/vendorsdatatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
            <link href="{{url('/')}}/vendorsdatatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
            <link href="{{url('/')}}/vendorsdatatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
            <link href="{{url('/')}}/vendorsdatatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
            <link href="{{url('/')}}/vendorsdatatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
            <link href="{{url('/')}}/build/css/custom.min.css" rel="stylesheet">
        @endsection
        @section('content')
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Ticket ID</th>
                                <th>Subject</th>
                                <th>Priority</th>
                                <th>Department</th>
                                <th>Status</th>
                                <th>Last Update</th>
                                <th>Ticket Created</th>
                                <th>Description</th>
                                <th>Content</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($listactive as $dbshow)
                            <tr>
                                <td>{{$dbshow -> id}}</td>
                                <td style="cursor: pointer;" onClick="document.location = 'ticket/edit/{{$dbshow -> id}}';">{{$dbshow -> subject}}</td>
                                <td style="cursor: pointer;" onClick="document.location = 'ticket/edit/{{$dbshow -> id}}';">{{$dbshow -> priority}}
                                <td style="cursor: pointer;" onClick="document.location = 'ticket/edit/{{$dbshow -> id}}';">{{$dbshow -> department}}</td>
                                <td style="cursor: pointer;" onClick="document.location = 'ticket/edit/{{$dbshow -> id}}';">{{$dbshow -> status}}</td>
                                <td style="cursor: pointer;" onClick="document.location = 'ticket/edit/{{$dbshow -> id}}';">{{$dbshow -> updated_at}}</td>
                                <td style="cursor: pointer;" onClick="document.location = 'ticket/edit/{{$dbshow -> id}}';">{{$dbshow -> created_at}}</td>
                                <td>{{$dbshow -> desc}}</td>
                                <td><img src="{{url('/')}}/{{$dbshow -> img}}" class="img-responsive"></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        @endsection
        @section('footer')
            footer
        @endsection
        @section('jscript')
    js ends<br>
    <script src="{{url('/')}}/vendors/jquery/dist/jquery.min.js"></script>
    <script src="{{url('/')}}/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="{{url('/')}}/vendors/fastclick/lib/fastclick.js"></script>
    <script src="{{url('/')}}/vendors/nprogress/nprogress.js"></script>
    <script src="{{url('/')}}/vendors/iCheck/icheck.min.js"></script>
    <script src="{{url('/')}}/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{url('/')}}/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="{{url('/')}}/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{url('/')}}/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="{{url('/')}}/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="{{url('/')}}/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="{{url('/')}}/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="{{url('/')}}/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="{{url('/')}}/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="{{url('/')}}/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{url('/')}}/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="{{url('/')}}/vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="{{url('/')}}/vendors/jszip/dist/jszip.min.js"></script>
    <script src="{{url('/')}}/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="{{url('/')}}/vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{url('/')}}/build/js/custom.min.js"></script>

    <!-- Datatables -->
    <script>
        $(document).ready(function() {
            var handleDataTableButtons = function() {
                if ($("#datatable-buttons").length) {
                    $("#datatable-buttons").DataTable({
                        dom: "Bfrtip",
                        buttons: [
                            {
                                extend: "copy",
                                className: "btn-sm"
                            },
                            {
                                extend: "csv",
                                className: "btn-sm"
                            },
                            {
                                extend: "excel",
                                className: "btn-sm"
                            },
                            {
                                extend: "pdfHtml5",
                                className: "btn-sm"
                            },
                            {
                                extend: "print",
                                className: "btn-sm"
                            },
                        ],
                        responsive: true
                    });
                }
            };

            TableManageButtons = function() {
                "use strict";
                return {
                    init: function() {
                        handleDataTableButtons();
                    }
                };
            }();

            $('#datatable').dataTable();

            $('#datatable-keytable').DataTable({
                keys: true
            });

            $('#datatable-responsive').DataTable();

            $('#datatable-scroller').DataTable({
                ajax: "js/datatables/json/scroller-demo.json",
                deferRender: true,
                scrollY: 380,
                scrollCollapse: true,
                scroller: true
            });

            $('#datatable-fixed-header').DataTable({
                fixedHeader: true
            });

            var $datatable = $('#datatable-checkbox');

            $datatable.dataTable({
                'order': [[ 1, 'asc' ]],
                'columnDefs': [
                    { orderable: false, targets: [0] }
                ]
            });
            $datatable.on('draw.dt', function() {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_flat-green'
                });
            });

            TableManageButtons.init();
        });
    </script>
    <!-- /Datatables -->
@endsection