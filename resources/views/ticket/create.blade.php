@extends('layout')
@section('xtra')
    <link href="{{url('/')}}/vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <link href="{{url('/')}}/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="{{url('/')}}/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <script language="javascript">
        function filecheck() {
            if (document.getElementById('fileclick').value <> "")) {
                alert(document.getElementById('fileclick').value));
                return true;
            } else {
                alert("Blank");
                return false;
            }
        }
    </script>
@endsection
@section('content')
<div class="container body">
    <div class="main_container">
        <div class="col-md-12 col-sm-12 col-lg-12 x_panel clo" role="main">
            <h4><strong>CREATE NEW TICKET</strong></h4>
        </div>
        <form action="/post" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <!-- description & product name -->
        <div class="clo" role="main">
            <form>
                <div class="clearfix"></div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">

                            <div class="form-group">
                                <label class="control-label col-md-2 col-sm-2 col-xs-12 h4">Subject</label>
                                <div class="col-md-10 col-sm-10 col-xs-12">
                                    <input required type="text" name="txtsubject" class="form-control">
                                </div>
                            </div>


                        </div>
                    </div></div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title"><h2>Ticket Description</h2><div class="clearfix"></div></div>
                        <div class="x_content">
                            <div id="alerts"></div>
                            <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor">
                                <div class="btn-group">
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                    </ul>
                                </div>
                                <div class="btn-group">
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a data-edit="fontSize 5"><p style="font-size:17px">Huge</p></a></li>
                                        <li><a data-edit="fontSize 3"><p style="font-size:14px">Normal</p></a></li>
                                        <li><a data-edit="fontSize 1"><p style="font-size:11px">Small</p></a></li>
                                    </ul>
                                </div>
                                <div class="btn-group">
                                    <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                                    <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                                    <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                                    <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                                    <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                                    <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
                                    <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                                    <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                                    <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                                    <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
                                    <div class="dropdown-menu input-append">
                                        <input class="span2" placeholder="URL" type="text" data-edit="createLink" />
                                        <button class="btn" type="button">Add</button>
                                    </div>
                                    <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="fa fa-picture-o"></i></a>
                                    <input type="file" onClick="return filecheck()" name="file" id="fileclick" data-target="#pictureBtn" data-role="magic-overlay" />
                                </div>
                                <div class="btn-group">
                                    <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                                    <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                                </div>
                            </div>

                            <div id="editor" class="editor-wrapper"></div>
                            <textarea name="descr" id="descr" style="z-index: -1;position: absolute; top: 3px;"></textarea>

                        </div>
                    </div>
                </div>
                <input type="hidden" name="txtprocess" value="Open">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel"><div class="x_content">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <select class="form-control" name="priority" required>
                            <option value="Medium">Priority option</option>
                            <option value="Low">Low</option>
                            <option value="Medium">Medium</option>
                            <option value="High">High</option>
                            <option value="Urgent">Urgent</option>
                        </select>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <select class="form-control" name="department" required>
                            <option value="other">Department</option>
                            <option value="Web">Web Developer</option>
                            <option value="Content">Content Team</option>
                        </select>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <input type="submit" value="Submit New Ticket" class="form-control btn btn-primary">
                    </div>
                    </div></div>
                </div>
             </div>
        </form>
    </div>
</div>
@endsection
@section('jscript')
            <script src="{{url('/')}}/vendors/jquery/dist/jquery.min.js"></script>
            <script src="{{url('/')}}/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
            <script src="{{url('/')}}/vendors/fastclick/lib/fastclick.js"></script>
            <script src="{{url('/')}}/vendors/nprogress/nprogress.js"></script>
            <script src="{{url('/')}}/build/js/custom.min.js"></script>

        <script src="{{url('/')}}/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
        <script src="{{url('/')}}/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
        <script src="{{url('/')}}/vendors/google-code-prettify/src/prettify.js"></script>
        <script src="{{url('/')}}/js/custom.min.js"></script>
        <!-- bootstrap-wysiwyg -->
        <script>
            $(document).ready(function() {
                function initToolbarBootstrapBindings() {
                    var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                            'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                            'Times New Roman', 'Verdana'
                        ],
                        fontTarget = $('[title=Font]').siblings('.dropdown-menu');
                    $.each(fonts, function(idx, fontName) {
                        fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
                    });
                    $('a[title]').tooltip({
                        container: 'body'
                    });
                    $('.dropdown-menu input').click(function() {
                        return false;
                    })
                        .change(function() {
                            $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
                        })
                        .keydown('esc', function() {
                            this.value = '';
                            $(this).change();
                        });

                    $('[data-role=magic-overlay]').each(function() {
                        var overlay = $(this),
                            target = $(overlay.data('target'));
                        overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
                    });

                    if ("onwebkitspeechchange" in document.createElement("input")) {
                        var editorOffset = $('#editor').offset();

                        $('.voiceBtn').css('position', 'absolute').offset({
                            top: editorOffset.top,
                            left: editorOffset.left + $('#editor').innerWidth() - 35
                        });
                    } else {
                        $('.voiceBtn').hide();
                    }
                }

                function showErrorAlert(reason, detail) {
                    var msg = '';
                    if (reason === 'unsupported-file-type') {
                        msg = "Unsupported format " + detail;
                    } else {
                        console.log("error uploading file", reason, detail);
                    }
                    $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
                }

                initToolbarBootstrapBindings();

                $('#editor').wysiwyg({
                    fileUploadError: showErrorAlert
                });

                window.prettyPrint;
                prettyPrint();
            });
        </script>
        <!-- /bootstrap-wysiwyg -->
@endsection