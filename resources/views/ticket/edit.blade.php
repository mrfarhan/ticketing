@extends('layout')
@section('xtra')
    <link href="{{url('/')}}/vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <link href="{{url('/')}}/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="{{url('/')}}/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class="col-md-12 col-sm-12 col-lg-12 x_panel clo" role="main">
        <h4><strong>EDIT TICKETS</strong></h4>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <img src="{{url('/')}}/{{$editlist -> img}}" class="img-responsive">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="list-inline prod_color">Ticket Priority :
                        @if ($editlist -> priority === 'Low')
                            <div value="Low" class="btn btn-round btn-success" title="{{$editlist -> priority}}"></div>
                        @elseif ($editlist -> priority === 'Medium')
                            <input type="button" value="Medium" class="btn btn-round btn-info"
                        @elseif ($editlist -> priority === 'High')
                            <input type="button" value="High" class="btn btn-round btn-warning"
                        @else
                            <input type="button" value="Urgent" class="btn btn-round btn-danger"
                        @endif
                        title="{{$editlist -> priority}}">

                            Ticket Status :
                        @if ($editlist -> status === 'Open')
                            <input type="button" value="Process" class="btn btn-round btn-warning" title="Process">
                        @else
                            <input type="button" value="Close" class="btn btn-round btn-success" title="Closed">
                        @endif
                            Department : <h3 style="display: inline-block!important; color: green">{{$editlist -> department}}</h3>
                            <span class="right" style="margin-top: 11px;">Option :
                                <select name="statusoption">
                                    <option value="{{$editlist -> status}}">Seletced {{$editlist -> status }}</option>
                                    <option value="Close">Close</option>
                                    <option value="Open">Open</option>
                                </select>
                            </span>
                    </div>

                    <div class="tile_header"><h4>{{$editlist -> subject}}</h4></div>
                    <p class="description">{{$editlist -> desc}}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content"><h3>From:</h3>
                <p>for each</p>
            </div>
        </div>
    </div>
    <form  action="/edit" method="POST">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content"><h3>Reply to the ticket :</h3>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <div id="alerts"></div>
                            <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor">
                                <div class="btn-group">
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                    </ul>
                                </div>
                                <div class="btn-group">
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a data-edit="fontSize 5"><p style="font-size:17px">Huge</p></a></li>
                                        <li><a data-edit="fontSize 3"><p style="font-size:14px">Normal</p></a></li>
                                        <li><a data-edit="fontSize 1"><p style="font-size:11px">Small</p></a></li>
                                    </ul>
                                </div>
                                <div class="btn-group">
                                    <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                                    <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                                    <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                                    <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                                    <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                                    <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
                                    <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                                    <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                                    <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                                    <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
                                    <div class="dropdown-menu input-append">
                                        <input class="span2" placeholder="URL" type="text" data-edit="createLink" />
                                        <button class="btn" type="button">Add</button>
                                    </div>
                                    <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="fa fa-picture-o"></i></a>
                                    <input type="file" onClick="return filecheck()" name="file" id="fileclick" data-target="#pictureBtn" data-role="magic-overlay" />
                                </div>
                                <div class="btn-group">
                                    <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                                    <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                                </div>
                            </div>

                            <div id="editor" class="editor-wrapper"></div>
                            <textarea name="descr" id="descr" style="z-index: -1;position: absolute; top: 3px;"></textarea>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input type="submit" value="Reply to the ticket" class="form-control btn btn-primary">
            </div>
        </div>
    </div>
    </form>
@endsection
@section('jscript')
    <script src="{{url('/')}}/vendors/jquery/dist/jquery.min.js"></script>
    <script src="{{url('/')}}/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="{{url('/')}}/vendors/fastclick/lib/fastclick.js"></script>
    <script src="{{url('/')}}/vendors/nprogress/nprogress.js"></script>
    <script src="{{url('/')}}/build/js/custom.min.js"></script>

    <script src="{{url('/')}}/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="{{url('/')}}/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="{{url('/')}}/vendors/google-code-prettify/src/prettify.js"></script>
    <script src="{{url('/')}}/js/custom.min.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script>
        $(document).ready(function() {
            function initToolbarBootstrapBindings() {
                var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                        'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                        'Times New Roman', 'Verdana'
                    ],
                    fontTarget = $('[title=Font]').siblings('.dropdown-menu');
                $.each(fonts, function(idx, fontName) {
                    fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
                });
                $('a[title]').tooltip({
                    container: 'body'
                });
                $('.dropdown-menu input').click(function() {
                    return false;
                })
                    .change(function() {
                        $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
                    })
                    .keydown('esc', function() {
                        this.value = '';
                        $(this).change();
                    });

                $('[data-role=magic-overlay]').each(function() {
                    var overlay = $(this),
                        target = $(overlay.data('target'));
                    overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
                });

                if ("onwebkitspeechchange" in document.createElement("input")) {
                    var editorOffset = $('#editor').offset();

                    $('.voiceBtn').css('position', 'absolute').offset({
                        top: editorOffset.top,
                        left: editorOffset.left + $('#editor').innerWidth() - 35
                    });
                } else {
                    $('.voiceBtn').hide();
                }
            }

            function showErrorAlert(reason, detail) {
                var msg = '';
                if (reason === 'unsupported-file-type') {
                    msg = "Unsupported format " + detail;
                } else {
                    console.log("error uploading file", reason, detail);
                }
                $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                    '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
            }

            initToolbarBootstrapBindings();

            $('#editor').wysiwyg({
                fileUploadError: showErrorAlert
            });

            window.prettyPrint;
            prettyPrint();
        });
    </script>
    <!-- /bootstrap-wysiwyg -->
@endsection