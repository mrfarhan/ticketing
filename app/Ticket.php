<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'issue_ticket';
    protected $fillable = [
        'txtsubject',
        'descr',
        'priority',
        'department',
        'status'
    ];

    protected $guarded = [
//        'id'
    ];
//    public $created_at = false;
//    public $updated_at = false;
    public $timestamps = true;
}
