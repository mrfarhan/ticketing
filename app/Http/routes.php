<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('ticket.index');
//});
//Route::get('/wel', function () {
//    return view('Welcome');
//});
//Route::get('/complete', function () {
//    return view('ticket.complete');
//});
//Route::get('/create', function () {
//    return view('ticket.create');
//});
//Route::get('/setting', function () {
//    return view('setting.index');
//});
//Route::get('/logout', function () {
//    return view('setting.auth');
//});

Route::get('/','TicketController@index');
Route::get('/active','TicketController@active');
Route::get('/ticket','TicketController@index');
Route::post('/post','TicketController@create');
Route::get('/create','TicketController@post');
Route::get('/complete','TicketController@complete');
Route::get('/ticket/edit/{tak}',[
    'uses' => 'TicketController@edit',
    'as' => 'urledit',
]);