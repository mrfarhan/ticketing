<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Ticket;
use Carbon\carbon;
use Storage;

class TicketController extends Controller
{
    public function index()
    {
        $listactive = Ticket::all();
        return view('ticket.index',compact('listactive'));
    }
    public function active()
    {
        $listactive = Ticket::where('status','Open')->orderBy('id','desc')->get();
        return view('ticket.active',compact('listactive'));
    }
    public function complete()
    {
        $listactive = Ticket::where('status','Close')->orderBy('created_at','desc')->get();
        return view('ticket.complete',compact('listactive'));
    }

    public function checkTicket($status, $filter)
    {
        if(!(isset($filter))) {
            $filter = 'desc';
        }
        $tickets = Ticket::where('status', $status)->orderBy('created_at', $filter)->get();
    }

    public function post()
    {
        return view('ticket.create');

    }
    public function edit($id)
    {
        $editlist = Ticket::find($id);
//        dd(request()->all($editlist));
        return view('ticket.edit',compact('editlist'));
    }
    public function create(Request $request)
    {
//        dd(request()->all());
//    public function descstring($string){
//        $new = htmlspecialchars(trim($sting),ENT_QUOTES);
//        return $new
//    }
        $saveval = new Ticket;
        $saveval->subject = request('txtsubject');
        $saveval->desc = request('descr');
//        $saveval->desc = descstring(request['descr']);
        $saveval->priority = request('priority');
        $saveval->department = request('department');
        $saveval->status = request('txtprocess');
//        $nameimg = $request->file('file')->getClientOriginalName();
        $name1 = Carbon::now();
        $name1 = $name1->toDateTimeString();
        $name1 = str_replace(" ","_",$name1);
        $name1 = str_replace(":","_",$name1);
        $name1 = str_replace("-","_",$name1);
//        dd($name1);
        if(request()->file('file')) {
            $file = $request->file('file');
            $ext = $file->extension();
            $file->move("img", $name1 .".{$ext}");
            $pathx = "img/". $name1 .".{$ext}";
//            dd($pathx);
            $saveval->img = $pathx;
//            $saveval->desc = $file;
        }
        $saveval->save();
        return redirect('/ticket');
    }
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        /**
         * Extract the ID
         * Find the ticket using the ID
         * Ticket
         * $flight = App\Flight::find(1);
         * $flight->name = 'New Flight Name';
         * $flight->save();
         */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
